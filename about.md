---
layout: page
title: About
permalink: /about/
---

Web developer with 6 years of experience who loves building things which help others. Pragmatic web generalist.

