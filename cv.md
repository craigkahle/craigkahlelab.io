---
layout: page
title: Resume
permalink: /cv/
---

##### I am a web developer with 6 years of experience who loves building things which help others. Pragmatic web generalist.

### Technologies

- Ruby / Rails
- JavaScript, ES6 / Vue, React
- PostgreSQL
- PHP, Laravel
- Ansible
- ElasticSearch
- AWS


### Work Experience

#### SXSW**Front End / Back End - July 2016 - Present**

- Rebuild of schedule.sxsw.com and related mobile APIs
- Rebuild of a Wristband distribution system
- Internal coupon distribution system buildout and front end
- Bulk checkout in cart.sxsw.com and switch to Spreedly / Chase orbital processing.
- ME convention cart fork launch and APIs with 3rd party ticketing providers

#### Mattr**Front End / Back End - Contract - September 2015 - March 2016**
- Contributed to a rebuild of 2 internal admin interfaces in React / Redux using ES6- Wrote API enpoints Rails that integrate with React front end- Added features to existing app using Handlebars templates / helpers and jQuery	#### Crispy Interactive
**Front End / Back End - June 2014 - August 2015**
- Front End / Fullstack developer- Built front-ends for homestory.co and the gymboree.com rebrand - Created an API in Laravel that is consumed by a Angular UI- Built a hybrid application using Ionic that integrated with Discourse in 3 months but was ultimately a failure.

#### University of Texas at Austin
**Front End - November 2012 - March 2014**
- Built cns.utexas.edu with another developer inside of 6 months and managed it  - Developed custom templates on the Joomla platform - Built a fully responsive theme and leverage caching solutions to reduce mobile load times- Created an admin area with user and style guides for faculty managed content updates- Built and managed 4 other subdomain sites   ### Education
**Temple University**
Philadelphia, PA  | 3.5 GPA | Class of February 2012 | B. A . Broadcasting, Telecommunications and Mass Media**Drexel University**
Philadelphia, PA | September 2007- Jan 2009 | Graphic Design

### Activities
**Public Service**
Gold Comm. Service Award -100 hours | Summers of 2004 | 2005 | 2006 Habitat for Humanity | Appalachia Service Project
