---
layout: post
title:  "Which damn editor should I use?"
date:   2018-08-07 14:45:40 -0500
categories: setup
---

I was asked by a friend recently about which editor to use when learning web development. This may be common knowledge to more experience folks, but meant to be a guide for people new to programming.

First things first is to determine what you are trying to do. This applies to many problems in development, and is very relevant here. I will go through each major editor and list which groups it is most applicable to.

### Sublime

Sublime is the best stand alone editor today. It has been around for over a decade and excels at effeciently editing text.

Pros:
  - Small  memory footprint and
  - Fast

Cons:
  - Requires a bit more out of the box configuration

**Great For:** Front end, HTML, ad hoc script editing, general web development


### Atom

The front end king of editors at this moment of writing. Gorgeous UI, and a great plugin ecosystem makes Atom a great choice for anyone who wants more of an IDE feel than Sublime.

Pros:
  - Great look and feel
  - Rich plugin ecosystem

Cons:
  - High memory footprint.
  - Can struggle with large files at times.

**Great For:** Front end, HTML, general web development


### VS Code

Pros:
  - Full featured editor with IDE features
  - Rich ecosystem similar to Atom
  - Deploy and git features baked in

Cons:
  - High memory footprint
  - Ideal for C# development, but that has changed recently

**Great For:** C#, front end, backend web frameworks, Microsoft based development


### JetBrains

JetBrains offers IDEs for many language environments that have die hard fans. They offer extremely advanced editors for a small fee.

Pros:
  - Fully fledged IDE
  - Extensions are baked in

Cons:
  - Somewhat expensive subscription
  - Can feel heavy for new users

**Great For:** Backend web frameworks, advanced users

### Vim

Vim is a classic editor that is nearly 30 years old. It is offered via a CLI and standalone interface. Vim offers a great option for users who spend most of their time in the command line. Biased plug: If you are constantly using ssh and editing server configs and spend a lot of time in the terminal, vim + tmux is your best bet.

Pros:
  - Battle tested
  - Extensive language support
  - Rich plugin ecosystem

Cons:
  - Confusing to new users (quit is just :q!)
  - Requires extensive configuration
  - Learning curve

**Great For:** C, web frameworks, advanced users, fast context switching


### Conclusion

Early on, you simply can't go wrong with **Sublime** or **Atom**. I would still pick Sublime any day as it is super stable, never crashes and offers numbers customization options. If you are sshed into multiple servers and working across multiple projects, look no further than **Vim**. It is well worth the investment of time.
