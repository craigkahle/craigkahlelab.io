---
layout: post
title:  "On Bootcamps and Getting Started"
date:   2018-08-08
categories: opinion
---

My cousin emailed me asking where to start learning programming so here is the slightly altered and highly biased email that I sent regarding bootcamps and a pontentially different path forward.

First off, I would try some free, interactive code tutorials to get your feet wet and see if you enjoy doing this stuff before dropping a bunch of cash on a bootcamp. For the record: Both of my close friends who have enrolled, had negative experiences, but this isn't always the case.

**Bootcamps can vary wildly in their quality, but all of them charge you a pretty penny for a seat.. ($10k+ range)**

My opinion is to go with Ruby on Rails. It's battle tested, is easier (subjective) and more readable than NodeJS.

The JavsScript ecosystem is constantly changing and can be tough on newcomers who might get further with a batteries included solution.

**Rails is still, to this day, the go to web framework for rapid development and developer happiness™**


### Learning Resources

Start with HTML / CSS and see stuff move around on a page using an interavtive editor like [CodePen](http://codepen.io).

[Code School](https://www.codeschool.com/learn/html-
css)

- An excellent starting point for learning the basics of web technologies

[Codecademy](https://www.codecademy.com/)

- Short, but a great primer in various technologies.

[Rails for Zombies](http://railsforzombies.org/)

- A great and gentle intro into Rails for complete beginners.


Once you get HTML down, The Rails Tutorial is an incredible multi week book, that will show you how to build twitter basically.
[Rails Tutorial](https://www.railstutorial.org/book) (And it's free!!!!!!!)
I was lucky enough to mee Michael Hartl in Phoenix last year. What a nice guy!

**After Completing tutorials:**

- Have a simple project in mind, e.g a real estate listing site, and set out to finish building it, and do it.
- Get good at reading docs. Install a program like Dash and use Alfred to quickly query docs.
- Doing little small projects outside of tutorials helps level you up.

### Conclusion

There is a lot out there, but start with fundamentals and take one step at a time.

If you are determined, you can section off part of your day just as you would at a bootcamp and achieve similar if not better results. You are teaching yourself to learn and not ask someone for help when you get stuck.

The additional benefit of this is potential employers will see this in a very positive light as you are a self learner who will stand out from other candidates.

Hope that helps! Please leave a comment below if there is anything I should add.
